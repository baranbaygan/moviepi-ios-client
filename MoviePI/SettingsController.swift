//
//  SettingsController.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

class SettingsController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    weak var deviceIdText:UITextField!
    weak var statusText:UILabel!
    private var updateAvailable:Bool! = false
    private var deviceVersion:String! = ""
    private var latestVersion:String! = ""
    
    private var versionRef:Firebase?
    private var latestVersionRef:Firebase?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings";
        
        let aSelector : Selector = "tableViewTapped:"
        
        let recognizer = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
        // 4
        recognizer.cancelsTouchesInView = false
        recognizer.delegate = self
        tableView.addGestureRecognizer(recognizer)
        
        self.latestVersionRef = Firebase(url: Global.fbUrl + "/appVersion/version")
        self.latestVersionRef!.observeEventType(FEventType.Value, withBlock: { (snap:FDataSnapshot!) -> Void in
            
            if let version:String = snap.value as? String {
                self.latestVersion = version
            }
            
            if(self.latestVersion != self.deviceVersion) {
                self.updateAvailable = true
            } else {
                self.updateAvailable = false
            }
            
            self.tableView.reloadData()
            
        })
    }
    
    
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        self.deviceIdText.resignFirstResponder()
    }
    
    override func blockedWhenNoDevice() -> Bool {
        return false
    }
    
    func save() {
        let cell:UITableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))!
        let deviceIdTextField = cell.viewWithTag(1) as! UITextField
        
        self.deviceId = deviceIdTextField.text
    }
    
    func update() {
        var contentUrlRef = Firebase(url: Global.fbUrl + "/tvCommand/" + self.deviceId)
        contentUrlRef.setValue(["command":"update"])
    }
    
    override func didChangeDeviceId(deviceId: String) {
        super.didChangeDeviceId(deviceId)
        
        if(self.deviceIdText != nil) {
            self.deviceIdText.text = deviceId
        }
        
        if(self.versionRef != nil) {
            self.versionRef?.removeAllObservers()
            self.versionRef = nil
        }
        
        self.versionRef = Firebase(url: Global.fbUrl + "/version/" + deviceId)
        self.versionRef!.observeEventType(FEventType.Value, withBlock: { (snap:FDataSnapshot!) -> Void in
            
            if let version:String = snap.value as? String {
                self.deviceVersion = version
            }
            
            if(self.latestVersion != self.deviceVersion) {
                self.updateAvailable = true
            } else {
                self.updateAvailable = false
            }
            
            self.tableView.reloadData()
        })
        
        
    }
    
    override func didChangeConnectionState(state: ConnectionState) {
        
        super.didChangeConnectionState(state)
        
        if(self.statusText != nil) {
            self.statusText.text = self.getConnectionStateString(state)
        }
    }
    
    // UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0) {
            return 2
        } else if(section == 1) {
            return 1
        }
        
        if(self.updateAvailable == true) {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section==0) {
            if(indexPath.row==0) {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("RemoteIdCell", forIndexPath: indexPath) 
                let deviceIdText = cell.viewWithTag(1) as! UITextField
                deviceIdText.text = self.deviceId
                self.deviceIdText = deviceIdText
                
                return cell;
            } else {
                return tableView.dequeueReusableCellWithIdentifier("SaveCell", forIndexPath: indexPath) 
            }
        } else if(indexPath.section == 1) {
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("StatusCell", forIndexPath: indexPath) 
            self.statusText = cell.viewWithTag(1) as! UILabel
            return cell;
        } else if(indexPath.section == 3) {
            return tableView.dequeueReusableCellWithIdentifier("HistoryCell", forIndexPath: indexPath) 
        } else {
            
            if(indexPath.row == 0) {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("VersionCell", forIndexPath: indexPath) 
                cell.textLabel?.text = self.deviceVersion
                return cell
            } else {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("UpdateCell", forIndexPath: indexPath) 
                let versionLabel:UILabel = cell.viewWithTag(1) as! UILabel
                versionLabel.text = "Update to \(self.latestVersion)"
                return cell
            }
            
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0) {
            if(indexPath.row==1) {
                self.save()
            }
        } else if(indexPath.section==2) {
            if(indexPath.row==1) {
                self.update()
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if(indexPath.row==0) {
            return nil
        }
        return indexPath
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section==0) {
            return "Connect your device"
        } else if(section == 1) {
            return "Device status"
        } else if(section == 2) {
            return "Device version"
        }
        return nil
    }
    
    
}
