//
//  MainTabBarViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabItems = self.tabBar.items as? [UITabBarItem]
        let tabItem0 = tabItems[0] as UITabBarItem
        let tabItem1 = tabItems[1] as UITabBarItem
        let tabItem2 = tabItems[2] as UITabBarItem
        let tabItem3 = tabItems[3] as UITabBarItem
        let tabItem4 = tabItems[4] as UITabBarItem
        
        tabItem0.title = "Channels"
        tabItem0.image = UIImage(named: "home")
        tabItem1.title = "Now Playing"
        tabItem1.image = UIImage(named: "nowplaying")
        tabItem2.title = "History"
        tabItem2.image = UIImage(named: "history")
        tabItem3.title = "Library"
        tabItem3.image = UIImage(named: "library")
        tabItem4.title = "Settings"
        tabItem4.image = UIImage(named: "settings")
    }
    
    
}
