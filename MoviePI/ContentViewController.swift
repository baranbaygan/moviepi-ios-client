//
//  ContentViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

public class ContentViewController : BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    public var url:String!
    
    public override func viewDidLoad() {
        
        self.automaticallyAdjustsScrollViewInsets = true
        self.webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal
        
        let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let deviceId:String = defaults.stringForKey(Global.Keys.deviceId)!
        
        if(self.url==nil) {
            webView.loadRequest(NSURLRequest(URL: NSURL(string: "http://192.168.1.100:7676")!));
        }
        else {
            if(url.rangeOfString("?") == nil) {
                url = url + "?"
            }
            url = url + "&deviceId=" + deviceId
            webView.loadRequest(NSURLRequest(URL: NSURL(string: url)!));
        }
    }
    
    public override func viewDidAppear(animated: Bool) {
        let javascript = "javascript:moviepi.callOnResume()";
        self.webView.stringByEvaluatingJavaScriptFromString(javascript)
    }
    
    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        var url = request.URL!.absoluteString
        
        if(url.hasPrefix("mpi")==true) {
            
            self.handleJavascriptCommand(url);
            
            return true
        }
        
        return true
    }
    
    private func handleJavascriptCommand(url:String) {
        
        let json:String = url.componentsSeparatedByString("mpi://")[1]
        let jsonStr = json.stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        let data = jsonStr?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        
        let dict = parseJSON(data!)
        let commandStr:String = dict["command"] as! String!
        
        switch commandStr {
            
        case "openPage":
            handleOpenUrl(dict);
        case "addHome":
            handleAddHome(dict);
        case "urlRequest":
            handleUrlRequest(dict);
        default:
            handleTVCommand(dict);
        }
        
    }
    
    func handleAddHome(dict:NSDictionary) {
        
        // Check validity of bookmark dictionary
        if(dict["id"] == nil || dict["name"] == nil || dict["url"] == nil || dict["icon"] == nil) {
            return
        }
        if((dict["id"] as! String).characters.count < 10) {
            return
        }
        
        var alert = UIAlertController(title: "Do you want to add this to your home screen?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            
        }))
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Default, handler: { action in
            
            var defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            let deviceId:String = defaults.stringForKey(Global.Keys.deviceId)!
            
            let url:String! = "\(Global.fbUrl)/apps/\(deviceId)/installed/" + (dict["id"] as! String)
            var fbRef = Firebase(url: url)
            fbRef.setValue(dict)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func handleUrlRequest(dict:NSDictionary) {
        let url = NSURL(string: dict["url"] as! String)
        let callbackId = dict["callbackId"] as! String
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            
            dispatch_async(dispatch_get_main_queue(),{
                let resultStr = NSString(data: data!, encoding: NSUTF8StringEncoding)?.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                let javascript = "javascript:moviepi.onCallback('" + callbackId + "','" + resultStr! + "')";
                self.webView.stringByEvaluatingJavaScriptFromString(javascript)
            });
        }
        
        task.resume()
    }
    
    func handleTVCommand(dict:NSDictionary) {
        
        var defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let deviceId:String = defaults.stringForKey(Global.Keys.deviceId)!
        
        
        var connectionRef = Firebase(url: Global.fbUrl + "/online/" + deviceId)
        connectionRef.observeSingleEventOfType(.Value, withBlock: { snapshot -> Void in
            if(snapshot != nil && !(snapshot.value is NSNull)) {
                
                var commandUrlRef = Firebase(url: Global.fbUrl + "/tvCommand/" + deviceId)
                commandUrlRef.setValue(dict)
                
                if(dict["command"] != nil && dict["title"] != nil) {
                    var command = dict["command"] as! String
                    if(command.rangeOfString("play") != nil) {
                        
                        self.toastText("Playing " + (dict["title"] as! String))
                        
                    } else if(command.rangeOfString("download") != nil) {
                        self.toastText("Downloading " + (dict["title"] as! String))
                    } else if(command.rangeOfString("addToLibrary") != nil) {
                        self.toastText("Added to library: " + (dict["title"] as! String))
                    }
                }
                
            } else {
                //                var alert = UIAlertController(title: "Device is not connected", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
                //                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { action in
                //
                //                }))
                //                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
        
    }
    
    func handleOpenUrl(dict:NSDictionary) {
        let vc:ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController!
        vc.url = dict["url"] as! String!
        if(dict["title"] != nil) {
            vc.title = dict["title"] as? String
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func parseJSON(inputData: NSData) -> NSDictionary{
        var error: NSError?
        let boardsDictionary: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        
        return boardsDictionary
    }
    
}