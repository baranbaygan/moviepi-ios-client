//
//  NowPlayingControllerViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

class NowPlayingControllerViewController: BaseViewController {
    
    @IBOutlet weak var videoSlider: UISlider!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    
    @IBOutlet weak var btnMedia: UIBarButtonItem!
    @IBOutlet weak var viewStopped: UIView!
    
    var sliderDragging:Bool = false
    var isPaused:Bool = false
    var commandRef:Firebase = Firebase()
    var position:Double = 0;
    var duration:Double = 0;
    
    var media:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Now Playing";
        
        self.viewStopped.hidden = false
        self.btnMedia.enabled = false
        
    }
    
    override func didChangeDeviceId(deviceId: String) {
        
        super.didChangeDeviceId(deviceId)
        
        self.commandRef = Firebase(url: Global.fbUrl + "/tvCommand/" + deviceId)
        
        var nowPlayingMediaRef = Firebase(url: Global.fbUrl + "/nowplayingmedia/" + deviceId)
        nowPlayingMediaRef.observeEventType(.Value, withBlock: { (snap:FDataSnapshot!) -> Void in
            if let dict = snap.value as? NSDictionary {
                
                self.media = dict
                
                if(dict["title"] != nil) {
                    var title = dict["title"] as! String!
                    self.lblTitle.text = title
                }
                self.viewStopped.hidden = true
                self.btnMedia.enabled = true
            } else {
                self.viewStopped.hidden = false
                self.btnMedia.enabled = false
            }
        })
        
        
        var nowPlayingRef = Firebase(url: Global.fbUrl + "/nowplaying/" + deviceId)
        nowPlayingRef.observeEventType(.Value, withBlock: { (snap:FDataSnapshot!) -> Void in
            
            if let dict = snap.value as? NSDictionary {
                self.duration = dict["duration"]!.doubleValue!
                self.position = dict["position"]!.doubleValue!
                
                if(self.sliderDragging==false) {
                    self.lblProgress.text = "\(self.minuteStringFromDouble(self.position)) / \(self.minuteStringFromDouble(self.duration))"
                }
                
                var isPaused:Bool = dict["paused"]!.boolValue!
                self.isPaused = isPaused
                
                if(isPaused) {
                    self.btnPlay.setImage(UIImage(named: "play"), forState: UIControlState.Normal)
                } else {
                    self.btnPlay.setImage(UIImage(named: "pause"), forState: UIControlState.Normal)
                }
                
                
                if(self.sliderDragging==false) {
                    self.videoSlider.setValue(Float((self.position/self.duration)), animated: false)
                }
                self.btnPlay.enabled = true
                //self.btnStop.enabled = true
                self.btnForward.enabled = true
                self.btnBackward.enabled = true
                self.lblProgress.enabled = true
                self.videoSlider.enabled = true
                
            } else {
                
                self.btnPlay.enabled = false
                //self.btnStop.enabled = false
                self.btnForward.enabled = false
                self.btnBackward.enabled = false
                self.lblProgress.enabled = false
                self.videoSlider.enabled = false
            }
        })
    }
    
    
    @IBAction func mediaTapped(sender: AnyObject) {
        
        let vc:ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController!
        let url = self.media["containerUrl"] as! String!
        vc.url = url
        vc.title = self.media["title"] as! String!
        
        let tabController:UITabBarController = self.tabBarController!
        let firstNavController:UINavigationController = tabController.viewControllers?.first as! UINavigationController
        tabController.selectedIndex = 0
        firstNavController.pushViewController(vc, animated: true)
    }
    
    func minuteStringFromDouble(val:Double) -> String {
        
        let totalSecs = Int(val / 1000000)
        var mins = 0;
        if(totalSecs>=60) {
            mins = (totalSecs - (totalSecs % 60)) / 60
        } else {
            mins = 0
        }
        
        let secs = totalSecs % 60
        
        var minsStr:String = "\(mins)"
        var secStr = "\(secs)"
        
        if(minsStr.characters.count==1) {
            minsStr = "0" + minsStr
        }
        if(secStr.characters.count==1) {
            secStr = "0" + secStr
        }
        
        return "\(minsStr):\(secStr)"
    }
    
    @IBAction func sliderValueChanged(sender: AnyObject) {
        let targetPosition = Double(self.videoSlider.value) * self.duration
        self.lblProgress.text = "\(self.minuteStringFromDouble(targetPosition)) / \(self.minuteStringFromDouble(self.duration))"
    }
    
    @IBAction func sliderTouchDown(sender: AnyObject) {
        print("sliderTouchDown")
        self.sliderDragging = true;
    }
    @IBAction func sliderTouchUpOutside(sender: AnyObject) {
        print("sliderTouchUpOutside")
        self.sliderDragging = false;
    }
    @IBAction func sliderTouchUpInside(sender: AnyObject) {
        print("sliderTouchUpInside")
        self.sliderDragging = false;
        
        print("Slider value: \(self.videoSlider.value)")
        var targetPosition = Double(self.videoSlider.value) * self.duration
        var offset = (targetPosition - self.position) / 1000000;
        var command = ["command": "seek", "offset": "\(offset)"]
        self.commandRef.setValue(command)
    }
    
    // Buttons
    @IBAction func playTapped(sender: AnyObject) {
        print("playTapped")
        
        var command = ["command": "playPause"]
        self.commandRef.setValue(command)
    }
    @IBAction func stopTapped(sender: AnyObject) {
        print("stopTapped")
        
        var command = ["command": "stop"]
        self.commandRef.setValue(command)
    }
    @IBAction func backwardTapped(sender: AnyObject) {
        print("backwardTapped")
        
        var command = ["command": "seek", "offset": "-30"]
        self.commandRef.setValue(command)
    }
    @IBAction func forwardTapped(sender: AnyObject) {
        print("forwardTapped")
        
        var command = ["command": "seek", "offset": "30"]
        self.commandRef.setValue(command)
    }
}
