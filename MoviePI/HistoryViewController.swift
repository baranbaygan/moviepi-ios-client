//
//  HistoryViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 04/05/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

public class HistoryViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    private var historyRef:Firebase!
    private var historyItems:Array<NSDictionary>!

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblEmpty: UILabel!
    @IBOutlet weak var btnClear: UIBarButtonItem!
    
    public override func viewDidLoad() {
        
        self.title = NSLocalizedString("History", comment: "comment")
        self.btnClear.enabled = false
        self.lblEmpty.hidden = false
        self.tableView.hidden = true
    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath:NSIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
        
        reloadHistory()
    }
    
    private func reloadHistory() {
        if(self.historyRef == nil && self.deviceId != nil) {
            self.historyItems = Array();
            self.historyRef = Firebase(url: Global.fbUrl + "/history/" + self.deviceId);
            var query = self.historyRef.queryOrderedByChild("createDate").queryLimitedToLast(50);
                
            query.observeEventType(FEventType.ChildAdded, withBlock: { snap -> Void in
                if(snap != nil && !(snap.value is NSNull)) {
                    var historyItem:NSDictionary = snap.value as! NSDictionary
                    self.historyItems.insert(historyItem, atIndex: 0)
                }
            })
            query.observeEventType(FEventType.ChildRemoved, withBlock: { snap -> Void in
                if(snap != nil && !(snap.value is NSNull)) {
                    var historyItem:NSDictionary = snap.value as! NSDictionary
                    
                    if let i = self.historyItems.indexOf(historyItem) {
                        self.historyItems.removeAtIndex(i)
                    }
                }
            })
            
            self.historyRef.observeEventType(.Value, withBlock: { snapshow -> Void in
                //self.historyItemsReversed = self.historyItems.reverse()
                
                if(self.historyItems.count == 0) {
                    self.btnClear.enabled = false
                    self.lblEmpty.hidden = false
                    self.tableView.hidden = true
                } else {
                    self.btnClear.enabled = true
                    self.lblEmpty.hidden = true
                    self.tableView.hidden = false
                }
                
                self.tableView.reloadData()
            })
            
        }
    }
    
    public override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        //        self.historyItems = Array()
        //        self.tableView.reloadData()
    }
    
    @IBAction func clearBarButtonTapped(sender: AnyObject) {
        if(self.historyRef != nil) {
            var alert2 = UIAlertController(title: "Clear history?", message: nil, preferredStyle: .Alert)
            alert2.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                
            }))
            alert2.addAction(UIAlertAction(title: "Clear", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                self.historyRef.setValue(nil)
                self.historyRef.removeAllObservers()
                self.historyRef = nil
                self.reloadHistory()
            }))
            self.presentViewController(alert2, animated: true, completion: nil)
            
        }
        
    }
    // MARK: UITableViewDataSource
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyItems != nil ? self.historyItems.count : 0
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("HistoryCell", forIndexPath: indexPath) 
        
        if(historyItems == nil || historyItems.count == 0) {
            return cell
        }
        
        var imgIcon:UIImageView = cell.viewWithTag(1) as! UIImageView;
        var lblTitle:UILabel = cell.viewWithTag(2) as! UILabel;
        
        var historyItem = historyItems[indexPath.row] as NSDictionary
        
        lblTitle.text = historyItem["title"] as? String
        imgIcon.sd_setImageWithURL(NSURL(string: historyItem["icon"] as! String))
        
        return cell;
    }
    
    // MARK: UITableViewDelegate
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(historyItems == nil || historyItems.count == 0) {
            return
        }
        
        let historyItem = historyItems[indexPath.row] as NSDictionary
        
        let vc:ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController!
        let url = historyItem["containerUrl"] as! String!
        vc.url = url
        vc.title = historyItem["title"] as! String!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
