//
//  BaseViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 15/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

public class BaseViewController : UIViewController {
    
    
    public enum ConnectionState {
        case NotConnected
        case Connecting
        case Connected
    }
    private var remoteRef:Firebase!
    private var lblStatus:UILabel!
    private var disabledView:UIView!
    
    private var deviceOnlineRef:Firebase!
    
    private var timer:NSTimer!
    private var connectionState:ConnectionState! = ConnectionState.NotConnected
    
//    public var deviceConnected:Bool! {
//        get {
//            if(deviceLastPing == nil) {
//                return false
//            }
//            return (deviceLastPing.isGreaterThanDate(NSDate().addSeconds(-10)))
//        }
//    }
    
    public var deviceId:String! {
        didSet {
            if(deviceId != oldValue) {
                let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                defaults.setValue(deviceId, forKey: Global.Keys.deviceId)
                defaults.synchronize()
                
                
                didChangeConnectionState(ConnectionState.Connecting)
                didChangeDeviceId(deviceId)
                
            }
        }
    }
    
    
    public override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.disabledView = NSBundle.mainBundle().loadNibNamed("DisabledView", owner: self, options: nil)[0] as! UIView
        self.disabledView.hidden = true
        self.view.addSubview(self.disabledView)
        
        self.lblStatus = self.disabledView.viewWithTag(1) as! UILabel
        
        
        self.disabledView.translatesAutoresizingMaskIntoConstraints = false
        
        let constX = NSLayoutConstraint(item: self.disabledView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        view.addConstraint(constX)
        
        let constY = NSLayoutConstraint(item: self.disabledView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
        view.addConstraint(constY)
        
        let constW = NSLayoutConstraint(item: self.disabledView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 1)
        view.addConstraint(constW)
        //view.addConstraint(constW) also works
        
        let constH = NSLayoutConstraint(item: self.disabledView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 1)
        view.addConstraint(constH)
        
        didChangeConnectionState(ConnectionState.NotConnected)
    }

    public func toastText(text:String) {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Text
        loadingNotification.labelText = text
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(3 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        }
    }
    
//    func connectionCheckTick() {
//        // Something cool
//        handleLastPing()
//    }
    
//    func handleLastPing() {
//        if(self.deviceConnected==true) {
//            if(self.connectionState != .Connected) {
//                self.connectionState = .Connected
//                didChangeConnectionState(ConnectionState.Connected)
//            }
//        } else {
//            if(self.connectionState != .NotConnected) {
//                self.connectionState = .NotConnected
//                didChangeConnectionState(ConnectionState.NotConnected)
//            }
//        }
//    }
    
    public func didChangeConnectionState(state:ConnectionState) {
        //self.showOrHideDisabledView()
        
        //self.lblStatus.text = self.getConnectionStateString(state)
    }
    
    public func getConnectionStateString(state:ConnectionState) -> String {
        switch(state) {
        case .Connected:
            return "Connected"
        case .Connecting:
            return "Connecting"
        case .NotConnected:
            return "Not connected"
        default:
            return "Not connected"
        }
    }
    
    public func didChangeDeviceId(deviceId:String) {
        if(self.deviceOnlineRef != nil) {
            self.deviceOnlineRef.removeAllObservers()
        }
        
        self.deviceOnlineRef = Firebase(url: Global.fbUrl + "/online/" + deviceId)
        self.deviceOnlineRef.observeEventType(.Value, withBlock: { snapshot -> Void in
            if(snapshot != nil && !(snapshot.value is NSNull)) {
                self.connectionState = ConnectionState.Connected
            } else {
                self.connectionState = ConnectionState.NotConnected
            }
            self.didChangeConnectionState(self.connectionState)
        })
        
//        self.deviceLastPing = nil
//        self.devicePingRef = Firebase(url: Global.fbUrl + "/ping/" + deviceId)
//        self.devicePingRef.observeEventType(.Value, withBlock: { snapshot -> Void in
//            if(snapshot.value != nil) {
//                if let lastPing = snapshot.value as? NSTimeInterval {
//                    self.deviceLastPing = NSDate(timeIntervalSince1970: lastPing/1000)
//                } else {
//                    self.deviceLastPing = nil
//                }
//            } else {
//                self.deviceLastPing = nil
//            }
//            
//            self.handleLastPing()
//        })
        
//        if(self.timer != nil) {
//            self.timer.invalidate()
//            self.timer = nil
//        }
//        self.timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("connectionCheckTick"), userInfo: nil, repeats: true)

        
        
        
        doRemoteConnection()
    }
    
    private func doRemoteConnection() {
        
        print("doRemoteConnection")
        
        var defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        if(remoteRef != nil) {
            remoteRef.setValue(nil)
        }
        
        var remoteId = defaults.valueForKey(Global.Keys.remoteId) as? String
        remoteRef = Firebase(url: Global.fbUrl + "/remote/" + deviceId + "/" + remoteId!)
        let vers: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
        remoteRef.setValue(["os":"iOS", "version": vers!])
        remoteRef.onDisconnectRemoveValue()
    }
    
    public func blockedWhenNoDevice() -> Bool {
        return false
    }
    
    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        // Check device id
        let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let deviceId:String? = defaults.stringForKey(Global.Keys.deviceId)
        
        if(deviceId != nil && deviceId != self.deviceId){
            self.deviceId = deviceId
        }
        
        
        self.showOrHideDisabledView()
    }
    
    private func showOrHideDisabledView() {
//        self.disabledView.hidden = false
//        
//        if(blockedWhenNoDevice()) {
//            if((self.deviceId != nil && count(self.deviceId) > 0) && self.connectionState == ConnectionState.Connected) {
//                self.disabledView.hidden = true
//            } else {
//                self.disabledView.hidden = false
//            }
//        } else {
//            self.disabledView.hidden = true
//        }
    }
    
    
    public override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
}
