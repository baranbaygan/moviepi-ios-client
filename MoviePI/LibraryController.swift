//
//  LibraryController.swift
//  MoviePI
//
//  Created by Baran Baygan on 11/06/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit
import MBProgressHUD

public class LibraryController: BaseViewController {
    
    private var completedRef:Firebase!
    private var downloadingRef:Firebase!
    private var queuedRef:Firebase!
    
    private var completedItems:AnyObject!
    private var queuedItems:AnyObject!
    private var currentDownload:AnyObject!
    
    private var tableViewData:NSMutableArray!
    private var commandRef:Firebase = Firebase()
    
    @IBOutlet weak var tableView: UITableView!
    
    override public func viewDidLoad() {
        self.title = NSLocalizedString("Library", comment: "comment")
        self.tableViewData = NSMutableArray()
        self.tableView.allowsMultipleSelectionDuringEditing = false
        
    }
    
    func sorterForName(this:AnyObject, that:AnyObject) -> Bool {
        
        let a:NSDictionary = this as! NSDictionary
        let b:NSDictionary = that as! NSDictionary
        
        let nameA:String = a["name"] as! String
        let nameB:String = b["name"] as! String
        
        return nameA.compare(nameB, options:[],
            range: Range<String.Index>(start: nameA.startIndex,end: nameA.endIndex),
            locale: nil) == NSComparisonResult.OrderedAscending
    }
    
    override public func viewDidAppear(animated: Bool) {
        if(self.deviceId != nil) {
            
            self.commandRef = Firebase(url: Global.fbUrl + "/tvCommand/" + deviceId)
            
            self.completedRef = Firebase(url: Global.fbUrl + "/download/" + self.deviceId + "/completed");
            self.queuedRef = Firebase(url: Global.fbUrl + "/download/" + self.deviceId + "/queue");
            self.downloadingRef = Firebase(url: Global.fbUrl + "/download/" + self.deviceId + "/downloading");
            
            self.completedRef.observeEventType(FEventType.Value, withBlock: { (snap) -> Void in
                if(snap != nil && !(snap.value is NSNull)) {
                    var dict:NSDictionary = snap.value as! NSDictionary
                    
                    var items:Array<AnyObject> = Array<AnyObject>(dict.allValues)
                    items.sortInPlace(self.sorterForName)
                    self.completedItems = items
                    
                    self.buildData()
                }
            })
            self.queuedRef.observeEventType(FEventType.Value, withBlock: { (snap) -> Void in
                if(snap != nil && !(snap.value is NSNull)) {
                    var dict:NSDictionary = snap.value as! NSDictionary
                    
                    var items:Array<AnyObject> = Array<AnyObject>(dict.allValues)
                    items.sortInPlace(self.sorterForName)
                    self.queuedItems = items
                    
                    self.buildData()
                }
            })
            self.downloadingRef.observeEventType(FEventType.Value, withBlock: { (snap) -> Void in
                if(snap != nil && !(snap.value is NSNull)) {
                    
                    var dict:NSDictionary = snap.value as! NSDictionary
                    
                    if(self.currentDownload == nil && snap.value != nil || self.currentDownload != nil && snap.value == nil) {
                        
                        let items = [dict]
                        self.currentDownload = items
                        
                        self.buildData()
                    } else {
                        
                        let items = [dict]
                        self.currentDownload = items
                    }
                    
                    if let cell:UITableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))  {
                        if(cell.reuseIdentifier == "DownloadingCell") {
                            
                            let lblName = cell.viewWithTag(2) as! UILabel
                            let lblSpeed = cell.viewWithTag(3) as! UILabel
                            let progressView = cell.viewWithTag(4) as! UIProgressView
                            
                            lblName.text = dict["name"] as? String
                            lblSpeed.text = dict["speed"] as? String
                            
                            if(dict["downloaded"] != nil && dict["total"] != nil) {
                                var progress:Float = (dict["downloaded"] as! Float) / (dict["total"] as! Float)
                                progressView.setProgress(progress, animated: false)
                                
                                var progressInt:Int = Int(progress * 100)
                                
                                var progressStr = "%\(progressInt)"
                                var speedStr = (dict["speed"] as? String)
                                lblSpeed.text = speedStr!  + " - " + progressStr
                            } else {
                                progressView.setProgress(0, animated: false)
                            }
                        }
                    }
                    
                }else {
                    self.currentDownload = nil
                    self.buildData()
                }
            })
        }
    }
    
    private func buildData() {
        self.tableViewData = NSMutableArray()
        if(self.currentDownload != nil) {
            self.tableViewData.addObject(self.currentDownload)
        }
        if(self.completedItems != nil) {
            self.tableViewData.addObject(self.completedItems)
        }
        if(self.queuedItems != nil) {
            
            if(self.currentDownload != nil) {
                // Remove current downloading item from queued items
                var queuedItemsArray:Array<AnyObject> = self.queuedItems as! Array<AnyObject>
                var i = 0
                for item in queuedItemsArray {
                    
                    let values:NSDictionary = item as! NSDictionary
                    let url:String = values["url"] as! String
                    if(self.isDownloadingUrl(url)) {
                        
                        queuedItemsArray.removeAtIndex(i)
                        
                        if(queuedItemsArray.count==0) {
                            self.queuedItems = nil
                        } else {
                            self.queuedItems = queuedItemsArray
                        }
                        
                        break
                    }
                    
                    i++
                }
            }
            
            if(self.queuedItems != nil) {
                self.tableViewData.addObject(self.queuedItems)
            }
            
        }
        
        self.tableView.reloadData()
    }
    
    private func cellIdentifierForData(obj:AnyObject) -> String {
        
        
        
        var firstItem:NSDictionary
        let arrayObj = obj as? Array<AnyObject>
        firstItem = arrayObj?.first as! NSDictionary
        let testUrl:String = firstItem["url"] as! String
        
        if(self.isDownloadingUrl(testUrl)) {
            return "DownloadingCell"
        }
        
        
        
        //        if(self.currentDownload != nil && (self.currentDownload.firstItem["url"] as! String) == (obj.firstItem["url"] as! String)) {
        //            return "DownloadingCell"
        //        }
        
        if(self.currentDownload != nil && obj === self.currentDownload) {
            return "DownloadingCell"
        } else if(self.queuedItems != nil && obj === self.queuedItems) {
            return "QueuedCell";
        } else {
            return "CompletedCell";
        }
    }
    private func cellIdentifierForIndexPath(indexPath:NSIndexPath) -> String {
        
        let data:AnyObject = self.tableViewData[indexPath.section];
        return cellIdentifierForData(data)
    }
    private func dataForIndexPath(indexPath:NSIndexPath) -> NSDictionary {
        let data:AnyObject = self.tableViewData[indexPath.section];
        return data[indexPath.row] as! NSDictionary
    }
    
    private func isDownloadingUrl(url:String) -> Bool {
        if(self.currentDownload != nil) {
            
            let currentDownloadingUrl = ((self.currentDownload as! Array<AnyObject>).first as! NSDictionary)["url"] as! String
            if(currentDownloadingUrl == url) {
                return true
            }
        }
        return false
    }
    
    private func playMedia(media:NSDictionary, withSubtitleCode:String?) {
        
        var commandDict = Dictionary<String, String>()
        var path = media["path"] as? String
        
        if let files = media["files"] as? NSArray {
            
            for item in files {
                var fileInfo:NSDictionary = item as! NSDictionary
                var fileName = fileInfo["name"] as! String
                
                if(fileName.endsWith(".mp4") ||
                    fileName.endsWith(".avi") ||
                    fileName.endsWith(".mkv")) {
                        path = path! + "/" + (fileInfo["path"] as! String)
                        break
                }
            }
        }
        
        commandDict["command"] = "play"
        commandDict["url"] = media["url"] as? String
        commandDict["filePath"] = path
        commandDict["mediaType"] = media["mediaType"] as? String
        commandDict["icon"] = media["icon"] as? String
        commandDict["name"] = media["title"] as? String
        commandDict["title"] = media["title"] as? String
        commandDict["history"] = "false"
        commandDict["containerUrl"] = media["containerUrl"] as? String
        
        if(withSubtitleCode != nil) {
            commandDict["subtitleLangCode"] = withSubtitleCode
        }
        
        self.commandRef.setValue(commandDict)
        
        
        
        if(commandDict["command"] != nil && commandDict["title"] != nil) {
            
            var command:String = commandDict["command"]!
            if(command.rangeOfString("play") != nil) {
                self.toastText("Playing " + (commandDict["title"]!))
            }
        }
        
        
        
        
        
    }
    
    // MARK: UITableViewDataSource
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.tableViewData.count
    }
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items:AnyObject = self.tableViewData.objectAtIndex(section)
        if(self.currentDownload != nil && items === self.currentDownload) {
            return 1
        }
        
        return (items as! Array<AnyObject>).count
    }
    
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0 && indexPath.row == 0) {
            let a:Int = 5
        }
        
        var cellIdentifier:String = self.cellIdentifierForIndexPath(indexPath)
        
        NSLog("section \(indexPath.section) row: \(indexPath.row) cell: \(cellIdentifier)")
        
        var cell:UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) 
        
        let data:NSDictionary = dataForIndexPath(indexPath)
        
        if(cellIdentifier=="CompletedCell") {
            
            var imageView = cell.viewWithTag(1) as! UIImageView
            var label = cell.viewWithTag(2) as! UILabel
            
            label.text = data["name"] as? String
            imageView.sd_setImageWithURL(NSURL(string: data["icon"] as! String))
        } else if(cellIdentifier == "QueuedCell") {
            
            var imageView = cell.viewWithTag(1) as! UIImageView
            var label = cell.viewWithTag(2) as! UILabel
            
            label.text = data["name"] as? String
            imageView.sd_setImageWithURL(NSURL(string: data["icon"] as! String))
            
        } else {
            
            var imageView = cell.viewWithTag(1) as! UIImageView
            imageView.sd_setImageWithURL(NSURL(string: data["icon"] as! String))
            
            let lblName = cell.viewWithTag(2) as! UILabel
            let lblSpeed = cell.viewWithTag(3) as! UILabel
            let progressView = cell.viewWithTag(4) as! UIProgressView
            
            lblName.text = data["name"] as? String
            
            if(data["downloaded"] != nil && data["total"] != nil) {
                var progress:Float = (data["downloaded"] as! Float) / (data["total"] as! Float)
                progressView.setProgress(progress, animated: false)
                
                var progressInt:Int = Int(progress * 100)
                
                var progressStr = "%\(progressInt)"
                var speedStr = (data["speed"] as? String)
                lblSpeed.text = speedStr!  + " - " + progressStr
            } else {
                progressView.setProgress(0, animated: false)
            }
            
            
        }
        
        return cell
    }
    
    public func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let data:AnyObject = self.tableViewData[section] as AnyObject;
        let cellIdentifier:String = self.cellIdentifierForData(data)
        if(cellIdentifier == "CompletedCell") {
            return NSLocalizedString("Library", comment: "comment")
        } else if(cellIdentifier == "QueuedCell") {
            return NSLocalizedString("Queued", comment: "comment")
        } else {
            return NSLocalizedString("Downloading", comment: "comment")
        }
    }
    
    public func tableView(tableView: UITableView,
        canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
            return true
    }
    
    public func tableView(tableView: UITableView,
        commitEditingStyle editingStyle: UITableViewCellEditingStyle,
        forRowAtIndexPath indexPath: NSIndexPath) {
            
            var data = self.dataForIndexPath(indexPath)
            var mediaUrl:String = data["url"] as! String
            
            if(editingStyle == UITableViewCellEditingStyle.Delete) {
                var cellIdentifier = cellIdentifierForIndexPath(indexPath)
                if(cellIdentifier == "CompletedCell") {
                    var command = ["command": "deleteMedia", "url": mediaUrl]
                    self.commandRef.setValue(command)
                } else if(cellIdentifier == "QueuedCell") {
                    var command = ["command": "removeFromQueue", "url": mediaUrl]
                    self.commandRef.setValue(command)
                } else {
                    var command = ["command": "cancelCurrentDownload"]
                    self.commandRef.setValue(command)
                }
            }
    }
    
    
    // MARK: UITableViewDelegate
    
    public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cellIdentifier = cellIdentifierForIndexPath(indexPath)
        if(cellIdentifier == "CompletedCell") {
            return 77.0
        } else if(cellIdentifier == "QueuedCell") {
            return 77.0
        } else {
            return 80.0
        }
    }
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        var data = self.dataForIndexPath(indexPath)
        
        
        var name:String = data["name"] as! String
        var mediaType:String = data["mediaType"] as! String
        var mediaUrl:String = data["url"] as! String
        var containerUrl:String = data["containerUrl"] as! String
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var cellIdentifier = cellIdentifierForIndexPath(indexPath)
        if(cellIdentifier == "CompletedCell") {
            
            let optionmenu = UIAlertController(title: name, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
            let playAction = UIAlertAction(title: "Play", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                self.playMedia(data, withSubtitleCode: nil)
            })
            let playWithSubsAction = UIAlertAction(title: "Play With Subs", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                
                
                
                let subsOptionMenu = UIAlertController(title: "Select Language", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
                let eng = UIAlertAction(title: "English", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                    self.playMedia(data, withSubtitleCode: "eng")
                })
                let tur = UIAlertAction(title: "Turkish", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                    self.playMedia(data, withSubtitleCode: "tur")
                })
                let cancelSubAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction) -> Void in
                    
                })
                
                
                subsOptionMenu.addAction(eng)
                subsOptionMenu.addAction(tur)
                subsOptionMenu.addAction(cancelSubAction)
                
                self.presentViewController(subsOptionMenu, animated: true, completion: { () -> Void in
                    
                })
                
            })
            let detailsAction = UIAlertAction(title: "Details", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in
                var vc:ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController!
                vc.url = containerUrl
                vc.title = data["title"] as! String!
                self.navigationController?.pushViewController(vc, animated: true)
            })
            let removeAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Destructive, handler: { (alert:UIAlertAction) -> Void in
                
                
                let sureToRemoveMenu = UIAlertController(title: "Are you sure?", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
                let removeSureAction = UIAlertAction(title: "Remove Media", style: UIAlertActionStyle.Destructive, handler: { (alert:UIAlertAction) -> Void in
                    var command = ["command": "deleteMedia", "url": mediaUrl]
                    self.commandRef.setValue(command)
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction) -> Void in
                })
                sureToRemoveMenu.addAction(removeSureAction)
                sureToRemoveMenu.addAction(cancelAction)
                self.presentViewController(sureToRemoveMenu, animated: true, completion: nil)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction) -> Void in
            })
            optionmenu.addAction(playAction)
            if(mediaType == "magnet") {
                optionmenu.addAction(playWithSubsAction)
            }
            
            optionmenu.addAction(detailsAction)
            //optionmenu.addAction(removeAction)
            optionmenu.addAction(cancelAction)
            
            //            if(optionmenu.popoverPresentationController != nil) {
            //                optionmenu.popoverPresentationController!.sourceView = self.view
            //                optionmenu.popoverPresentationController!.sourceRect =
            //                    CGRectMake(p.x, p.y+50, 1.0, 1.0)
            //            }
            
            self.presentViewController(optionmenu, animated: true, completion: nil)
        } else if(cellIdentifier == "QueuedCell") {
//            // Queued cell
//            let optionmenu = UIAlertController(title: name, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
//            let cancelDownloadAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Destructive, handler: { (alert:UIAlertAction!) -> Void in
//                
//                var command = ["command": "removeFromQueue", "url": mediaUrl]
//                self.commandRef.setValue(command)
//            })
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction!) -> Void in
//                
//            })
//            optionmenu.addAction(cancelDownloadAction)
//            optionmenu.addAction(cancelAction)
//            self.presentViewController(optionmenu, animated: true, completion: nil)
        } else {
//            // Downloading cell
//            let optionmenu = UIAlertController(title: name, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
//            let cancelDownloadAction = UIAlertAction(title: "Cancel Download", style: UIAlertActionStyle.Destructive, handler: { (alert:UIAlertAction!) -> Void in
//                var command = ["command": "cancelCurrentDownload"]
//                self.commandRef.setValue(command)
//            })
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction!) -> Void in
//                
//            })
//            optionmenu.addAction(cancelDownloadAction)
//            optionmenu.addAction(cancelAction)
//            self.presentViewController(optionmenu, animated: true, completion: nil)
        }
    }
    
    
    
}
