//
//  Keys.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

struct Global {
    
    static let fbUrl:String = "https://moviepi.firebaseio.com"
    
    struct Keys {
        static let deviceId:String = "deviceId"
        static let remoteId:String = "remoteId"
    }

}
