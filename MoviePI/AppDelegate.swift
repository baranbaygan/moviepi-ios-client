//
//  AppDelegate.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var remoteId:String?
        
        remoteId = defaults.valueForKey(Global.Keys.remoteId) as? String
        
        if(remoteId == nil) {
            remoteId = randomStringWithLength(10) as String
            defaults.setObject(remoteId, forKey: Global.Keys.remoteId)
        }
        
        
        //defaults.setValue("MJGJBGN", forKey: Global.Keys.deviceId)
        //defaults.setValue("DXUVGFW", forKey: Global.Keys.deviceId)
        //defaults.setValue("ZXIVRVJ", forKey: Global.Keys.deviceId)
        
//        var deviceId:String! = defaults.valueForKey(Global.Keys.deviceId) as! String
//        var remoteRef = Firebase(url: Global.fbUrl + "/remote/" + deviceId + "/" + remoteId!)
//        let vers: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
//        remoteRef.setValue(["os":"iOS", "version": vers!])
//        remoteRef.onDisconnectRemoveValue()
        
        defaults.synchronize()
        
        
        //Firebase.setOption("persistence", to: true)
        
        return true
    }
    
    func randomStringWithLength (len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

