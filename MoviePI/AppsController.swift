//
//  FirstViewController.swift
//  MoviePI
//
//  Created by Baran Baygan on 02/04/15.
//  Copyright (c) 2015 Baran Baygan. All rights reserved.
//

import UIKit
import MBProgressHUD

class AppsController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var actionAppData:NSDictionary!
    
    private var apps:NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.collectionView.backgroundColor = UIColor.whiteColor()
        self.collectionView.alwaysBounceVertical = true
        
        
        let modelName = UIDevice.currentDevice().modelName
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        
        
        if(modelName == "iPhone 6 Plus" || modelName == "iPhone 6") {
            layout.itemSize = CGSizeMake(115, 140)
        } else {
            layout.itemSize = CGSizeMake(95, 110)
        }
        
        
        self.title = NSLocalizedString("Channels", comment: "comment")
        
        let longPresRec:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "longPressedItem:")
        longPresRec.delegate = self
        self.view.addGestureRecognizer(longPresRec)
    }
    
    @IBAction func addTapped(sender: AnyObject) {
        
        //1. Create the alert controller.
        var alert = UIAlertController(title: "Enter a Kudu channel url", message: "We also support goo.gl. You can just enter goo.gl code.", preferredStyle: .Alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = ""
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            
        }))
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] 
            print("Text field: \(textField.text)")
            
            var url:String! = textField.text
            
            if(url.characters.count < 5) {return}
            
            var first4 = url.substringToIndex(url.startIndex.advancedBy(4))
            
            if(first4 != "http") {
                url = "http://goo.gl/" + url
            }
            
            print("url: \(url)")
            
            var apiUrl = "http://boxe.azurewebsites.net/addapp?deviceId=\(self.deviceId)&url=\(url)"
            
            //let nsurl = NSURL(string: apiUrl)
            
            if let nsurl = NSURL(string: apiUrl) {
                
                
                
                
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Loading"
                
                let task = NSURLSession.sharedSession().dataTaskWithURL(nsurl) {(data, response, error) in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        var jsonResult: NSDictionary =
                        (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                        //println(NSString(data: data, encoding: NSUTF8StringEncoding))
                        
                        if(jsonResult["success"]?.boolValue == false) {
                            var errorCode = jsonResult["error"] as! String
                            var alert2 = UIAlertController(title: self.getAddErrorMessage(errorCode), message: nil, preferredStyle: .Alert)
                            alert2.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                
                            }))
                            self.presentViewController(alert2, animated: true, completion: nil)
                        }
                        
                        
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    }
                }
                
                task.resume()
                
                
            }
            
            
            
            
            
            
        }))
        
        
        
        // 4. Present the alert.
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func getAddErrorMessage(errorCode:String) -> String {
        switch(errorCode) {
        case "invalidBoxeApp":
            return "This is not a valid Kudu application"
        case "versionError":
            return "This Kudu app requires you to update your Kudu"
        default:
            return "Unknown error"
            
        }
    }
    
    func longPressedItem(gestureRecognizer:UILongPressGestureRecognizer) {
        if(gestureRecognizer.state != UIGestureRecognizerState.Began) {
            return
        }
        var p = gestureRecognizer.locationInView(self.collectionView)
        var indexPath:NSIndexPath? = self.collectionView.indexPathForItemAtPoint(p)
        
        if(indexPath != nil) {
            var cell:AppCell! = self.collectionView.cellForItemAtIndexPath(indexPath!) as! AppCell
            self.actionAppData = cell.app
            print("Long tapped")
            var title = cell.app["name"] as! String
            
            
            //let optionmenu = UIAlertController(title: nil, message: "Remove \(title)?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            let optionmenu = UIAlertController(title: nil, message: "Remove \(title)?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            let deleteAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Destructive, handler: { (alert:UIAlertAction) -> Void in
                let url:String! = "\(Global.fbUrl)/apps/\(self.deviceId)/installed/" + (self.actionAppData["id"] as! String)
                var fbRef = Firebase(url: url)
                fbRef.setValue(nil)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (alert:UIAlertAction) -> Void in
            })
            optionmenu.addAction(deleteAction)
            optionmenu.addAction(cancelAction)
            if(optionmenu.popoverPresentationController != nil) {
                optionmenu.popoverPresentationController!.sourceView = self.view
                optionmenu.popoverPresentationController!.sourceRect =
                    CGRectMake(p.x, p.y+50, 1.0, 1.0)
            }
            
            self.presentViewController(optionmenu, animated: true, completion: nil)
        }
    }
    
    //    -(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
    //    {
    //    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
    //    return;
    //    }
    //    CGPoint p = [gestureRecognizer locationInView:self.collectionView];
    //
    //    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    //    if (indexPath == nil){
    //    NSLog(@"couldn't find index path");
    //    } else {
    //    // get the cell at indexPath (the one you long pressed)
    //    UICollectionViewCell* cell =
    //    [self.collectionView cellForItemAtIndexPath:indexPath];
    //    // do stuff with the cell
    //    }
    //    }
    
    override func didChangeDeviceId(deviceId: String) {
        
        super.didChangeDeviceId(deviceId)
        
        var url:String = Global.fbUrl + "/apps/" + deviceId + "/installed"
        
        var appsRef = Firebase(url: url)
        appsRef.observeEventType(.Value, withBlock: { (snap:FDataSnapshot!) -> Void in
            if(snap != nil && !(snap.value is NSNull)) {
                self.apps = (snap.value as! NSDictionary).allValues
                
            } else {
                self.apps = []
            }
            
            self.collectionView.reloadData()
        })
    }
    
    
    // Data source
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell:AppCell = collectionView.dequeueReusableCellWithReuseIdentifier("AppsCell", forIndexPath: indexPath) as! AppCell
        
        var imageView:UIImageView = cell.viewWithTag(1) as! UIImageView!
        var nameLabel:UILabel = cell.viewWithTag(2) as! UILabel!
        
        var app:NSDictionary = self.apps.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell.app = app
        
        let iconUrl = NSURL(string: app["icon"] as! String)
        
        imageView.sd_setImageWithURL(iconUrl)
        nameLabel.text = app["name"] as! String!
        
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return apps != nil ? apps.count : 0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return apps != nil ? 1 : 0
    }
    
    // Delegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let app:NSDictionary = self.apps.objectAtIndex(indexPath.row) as! NSDictionary
        let vc:ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController!
        let url = app["url"] as! String!
        vc.url = url
        vc.title = app["name"] as! String!
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
}

